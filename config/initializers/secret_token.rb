# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Ticketee::Application.config.secret_key_base = '1ad3e7a7e301dd471ec05ad8890b70f31eff96f6c809b9d14af13ca7af1e9c0729c1170bae24ec9fb447adb206584b813a4338dd9722947cc675fad3e5562209'
